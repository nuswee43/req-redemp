import sys
import os
# jenkins exposes the workspace directory through env.
sys.path.append(os.environ['WORKSPACE'])
import requests
from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

# driver = webdriver.Chrome(chrome_options=chrome_option)

def goto_pwpp():
    url='https://api-staging.omise.co/charges'
    data={                                                                  
        'Omise-Version':'2017-11-02',
        'amount':2000,
        'currency':'thb',
        'return_uri':'http://www.omise.co',
        'source[type]':'points'
    }
    pwp=requests.post(url,auth=('skey_5gym665eziqdr2fb4j7',''),data=data)
    # logger.console(pwp.json())
    return pwp.json()['authorize_uri']


def input_info(base_url):
    chrome_option = Options()
    chrome_option.add_argument("--headless")
    driver = webdriver.Chrome(chrome_options=chrome_option)
    driver.get(base_url)
    driver.implicitly_wait(30)
    driver.find_element_by_xpath("//button[@id='homenextbtn']").click()
    sleep(1)
    username = driver.find_element_by_id("InputTextLast4Digits")
    username.send_keys('1234')
    phone = driver.find_element_by_id("InputTextMobileNo")
    phone.send_keys('951605333')
    sleep(1)
    driver.find_element_by_id("sendotpbtn").click()
    sleep(1)
    otp=driver.find_element_by_id("InputTextOTP")
    otp.send_keys('123456')
    driver.find_element_by_id("confirmotpbtn").click()
    sleep(2)
    driver.find_element_by_xpath("//*[@id='checkboxterm']").click()
    sleep(2)
    driver.find_element_by_xpath("//body/div[1]/div/div[1]/button[1]").click()
    sleep(3)
    driver.close()

for i in range(3200):
    url=goto_pwpp()
    input_info(url)
    # print(f'index:{i+1}')
