import requests
from robot.api import logger
class pwpp():
    def goto_pwpp(self,amount,key):
        url='https://api-staging.omise.co/charges'
        data={
            'Omise-Version':'2017-11-02',
            'amount':amount,
            'currency':'thb',
            'return_uri':'http://www.omise.co',
            'source[type]':'points'


        }
        pwp=requests.post(url,auth=(key,''),data=data)
        # logger.console(pwp.json())
        return pwp.json()['authorize_uri']


