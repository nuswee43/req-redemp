***Settings***
Library  Selenium2Library
Library  Collections
Library  BuiltIn
Library  String
Library  pwpp.py
Test Teardown  Close All Browsers

***Keywords***
get omise link
    [Arguments]  ${amount}  ${key}
    ${url}=  goto pwpp  ${amount}  ${key}  
    [Return]  ${url} 

Change Point pwp
    Wait Until Page Contains  Redeem your Citi Rewards  10s
    Click Element  //button[@id="homenextbtn"]
    Wait Until Page Contains  Enter your information  50s
    Input Text  //input[@id="InputTextLast4Digits"]  1234
    Input Text  //*[@id="InputTextMobileNo"]  951605333
    Sleep  1s
    Click Element  //*[@id="sendotpbtn"]
    Wait Until Element Is Visible  //*[@id="InputTextOTP"]  100s
    Sleep  2s
    Input Text  //*[@id="InputTextOTP"]  123456
    Sleep  1s
    Click Element  //*[@id="confirmotpbtn"]
    Wait Until Page Contains  Confirm payment  100s
    Select Checkbox  //*[@id="checkboxterm"]
    Sleep  2s
    Click Element  //body/div[1]/div/div[1]/button[1]
    Wait Until Page Contains  Accept and process payments  100s

Request pwp 
    ${url}=  get omise link  2000  skey_5gym665eziqdr2fb4j7
    Open Browser  ${url}  ${browser}
    Sleep  1.5s
    Change Point pwp
    Close All Browsers

***Variables***
${browser}  Headless Chrome
# ${browser}  chrome

***Test Cases***
Mock
    FOR  ${index}  IN RANGE  1  2900
        Request pwp
        Log To Console  ${index}
    END